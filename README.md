# AllMusic_Client
AllMusic配套mod  

AllMusic本体插件：https://github.com/Coloryr/AllMusic_Server

AllMusic Bamboo Music Fork插件（自定义音乐API）：https://gitlab.com/wifi-left/mcbamboomusic

下载地址：[https://modrinth.com/mod/allmusic-modified-client](https://modrinth.com/mod/allmusic-modified-client)
