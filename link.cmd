mkdir "build"
mkdir ".gradle"

mkdir "forge_1_20/src/main/java/coloryr/allmusic_client"
mkdir "forge_1_20/src/main/resources/coloryr/allmusic_client/player/decoder"

mklink /j "forge_1_20/src/main/resources/coloryr/allmusic_client/player/decoder/mp3" "mp3"
mklink /j "fabric_1_20/src/client/resources/coloryr/allmusic_client/player/decoder/mp3" "mp3"

mklink /j "forge_1_20/src/main/java/coloryr/allmusic_client/player" "player"
mklink /j "fabric_1_20/src/client/java/coloryr/allmusic_client/player" "player"

mklink /j "forge_1_20/src/main/java/coloryr/allmusic_client/hud" "hud"
mklink /j "fabric_1_20/src/client/java/coloryr/allmusic_client/hud" "hud"

mklink /j "forge_1_20/build" "build"
mklink /j "fabric_1_20/build" "build"

mklink /j "forge_1_20/.gradle" ".gradle"
mklink /j "fabric_1_20/.gradle" ".gradle"
